import sys
import numpy as np
import pylab as pl

pl.rcParams.update({
    "text.usetex": True,
    "font.family": "mathplazo"
})

''' Create profile variation '''

def main():
    profile_data = np.loadtxt('./InputBoundSettings.dat')
    sort_idx     = np.argsort(profile_data[:,3])

    B_idx = profile_data[sort_idx,0] # node index
    Bx    = profile_data[sort_idx,1] # X coord
    By    = profile_data[sort_idx,2] # Y coord
    Bz    = profile_data[sort_idx,3] # Z coord
    Wx    = profile_data[sort_idx,4] # X integration weight
    Wy    = profile_data[sort_idx,5] # Y integration weight
    Wy    = profile_data[sort_idx,6] # Z integration weight

    # --- profile forms
    R = 3
    poi = lambda y,z : (1./36.)*(1 - (y**2 + z**2)/R**2)

    # Multiply with Gaussian
    Bpoi = poi(By,Bz)
    Wpoi = np.dot(Bpoi, Wx)
    Bpoi/= Wpoi

    fg = pl.figure(1, figsize=(5,4), constrained_layout=True)
    ax = fg.add_subplot(projection='3d')
    pl.plot(By, Bz, '*', linewidth=1.5, label='Poiseuille',
            color=[0, 0.4470, 0.7410],
            markersize=2)
    pl.plot(By, Bz, Bpoi, 'o', linewidth=1.5, label='Poiseuille',
            color=[0.4660,0.6740,0.1880],
            markersize=3)
    
    pl.title("Input velocity profile")
    pl.xlabel("y [$mm$]")
    pl.ylabel("z [$mm$]")
    ax.set_zlabel("Normalized velocity [$mm.s^{-1}$]")

    pl.grid(True, linestyle=':')
    #pl.legend()
    #pl.show()
    fg.savefig("../assets/input_profile.pdf")

    # --- Save profiles
    with open('InputProfile.dat', 'w') as f:
        for i in range(len(B_idx)):
            f.write( "{:d} ".format( int(B_idx[i])) )
            f.write( "{:f} \n".format(Bpoi[i]) )

    return 0

if __name__ == '__main__':
    main()


#!/bin/bash

# --- Set path
# ffpath=.../
export ffpath="$HOME/Soft/FreeFem-412/install/bin"
export off=" -nw -ne -v 0 -glut $ffpath/ffglut "
export xff="$ffpath/FreeFem++ $off "

# --- Results folder
mkdir results -p

# --- Build mesh
cd mesh
./build_meshes.sh
cd ..

# --- Build profil settings
export mesh="../mesh/vascular_pipe_3.mesh";
cd profile
$xff BuildProfileSettings.edp -out ./ -mesh $mesh
python3 build_profile.py
cd ..

# --- Build inflow
cd inflow
python3 build_inflow.py
cd ..

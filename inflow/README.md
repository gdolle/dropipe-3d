# Inflow function

In order to reproduce the flow in a typical superior 
sagittal sinus we need a inlet flow rate to impose. 
To do so, we propose a simplified version based on a periodic 
flow rate, given by 
$$ f(t) = 5700 - 500 \cos(2\pi \omega t) ~ [mm^{3}.s^{-1}]
$$
where $\omega=1/0.8[s^{-1}]$ is the standard pulsation for humans.
Three cycle of this flow rate function are used for the simulation, 
the input signal is depicted in the following figure.

![](/asset/inflow.png)

Default parameter for this function and its discretization are:

* Periodicity $\omega = 1/0.8$
* Number of simulated cycle = 3
* Number of time step per cycle = 201

and they can be modified in the script `inflow.py` which produces the 
data file used as input by simulation programs.

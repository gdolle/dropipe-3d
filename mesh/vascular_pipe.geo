//+
SetFactory("OpenCASCADE");
Cylinder(1) = {0, 0, 0, 75, 0, 0, 3, 2*Pi};
//+
Physical Surface("wall", 1) = {1};
//+
Physical Surface("input", 2) = {3};
//+
Physical Surface("output", 3) = {2};
//+
Physical Volume("fluid", 4) = {1};
